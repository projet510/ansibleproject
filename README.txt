Ce projet vise à developper un programme qui permet l'automatisation de commandes sur un pole de PC grâce à une interface Tkinter et Ansible.
Dans cette interface on peut selectionner plusieurs PC et plusieurs programmes parmis une liste.
On va ensuite presser le bouton "valider" ce qui va avoir pour effet de lancer les programmes sélectionnés sur les PC sélectionnés. 



    ****************************************    Prérequis:   ******************************************************** 

 

- Avoir les privilèges administrateur sur le système (sudo ou root).
- Avoir un accès internet.
- S'assurer que Python3 est installé sur votre machine: "python3 --version"
Si il n'est pas présent, veuillez l'installer grâce à la commande suivante : "sudo apt-get install python3"




    ************************************    Structure du projet    **************************************************




- Inventory : fichiers contenants les informations des différentes cibles de notre programme (pole de PC : IP/nom/username/password)


    .list_hosts.ini : bibliothéque de toutes nos cibles dans laquelle nous allons récuperer les informations nécéssaires(sous  la forme : nom/IP/username/password/rootPassword)

    .hosts.ini : bibliothéque dans laquelle nous allons déposer les informations concernant les cibles sur lesquelles on souhaite lancer les programmes (informations copiées du hosts.ini)




- Playbooks : playbooks Ansible permettant l'automatisations de commandes sur un pole de PC
    .nmap : installation de nmap
    .programme : vérification de la présence de certains programmes(curl,python3,git)
    .update : update du PC
    .WazuhAgent : Installation de Wazuh agent




- Scripts : main.py et fichiers nécéssaires à son fonctionnement


    .playbook : Bibliothéque de tous les programmes ansible que nous pouvons sélectionner (parmis la liste des playbooks ci-dessus) sous la forme du path d'execution ansible + path vers le fichier.yml (pathExe + pathProgramme + nom du programme : path/vers/ansible-playbook -i /path/vers/hosts.ini -v /path/vers/playbook.yml * nomDuScript). Le "*" permet une lecture facile du fichier txt avec python (charactere de séparation) 

    .commande : Bibliothéque contenant les commandes bash des programmes sélectionnés provenant du fichier playbook. Ce fichier est réécrit à chaque validation et se présente sous la forme suivante (path/vers/ansible-playbook -i /path/vers/hosts.ini -v /path/vers/playbook.yml)

    .main.py : Programme permettant le lancement de l'interface graphique qui nous permet de choisir les PC et les playbooks puis de lancer les programmes automatiquement sur tous les PC.



    **********************************    Execution du programme:    ************************************************



    1. Interface générale:


    Lorrsque vous allez executer le programme vous aurez une interface avec 3 boutons et un espace de texte.
    Ces 3 boutons sont : sélection PC, sélection Ansible et validation.
    Le bouton de sélection des PC permet de sélectionner un ou plusieurs PC grâce à des checkboxs.
    Le bouton de sélection des programme ansible permet de selectionner un ou plusieurs programmes à lancer sur les PC sélectionnés.
    Le bouton validation va lancer les différents programmes ansible sélectionnés sur tous les PC sélectionnés et va nous retourner le résultat des commandes dans la zone d'affichage texte. 



    2. Explication détaillée bouton "sélectionPC": 


    Ce bouton va faire apparaitre une autre fenêtre dans laquelle vont se trouver les checkBoxs reliées aux différents PCs. Ces checkboxs sont crées en se basant sur le fichier "list_hosts.ini" qui va contenir les différents PCs. Ce fichiers contient le nom, IP, username, password et rootPassword de chacun des PCs. 

    Vous allez ensuite pouvoir sélectionner les cases qui correspondent aux PCs cible que vous visez. Une fois votre sélection faite vous allez pouvoir cliquer sur valider afin de valider votre sélection. A ce moment la, les lignes correspondantes aux différents PC cochés vont être copiées du fichier "list_hosts.ini" vers le fichier "hosts.ini". Le fichier "hosts.ini" contient maintenant les informations des PC que vous avez sélectionnés.

    Vous pouvez donc si vous le souhaitez ajouter des PC dans le fichier "list_hosts.ini" en respectant le format utilisé(ils apparaitrons alors parmis la liste de checkboxs).

    Le fichier "hosts.ini" est lui réécrit à chaque validation, vous n'avez rien à écrire dedans.



    3. Explication détaillée bouton "sélectionPlaybook":


     Ce bouton va faire apparaitre une autre fenêtre dans laquelle vont se trouver les checkBoxs reliées aux différents scripts ansible. Ces checkboxs sont crées en se basant sur le fichier "playbook.txt" qui va contenir les différents path des différents scripts ansible et leur nom sous le format suivant : path/vers/ansible-playbook -i /path/vers/hosts.ini -v /path/vers/playbook.yml * nomDuScript. Le "*" permet une lecture facile du fichier txt avec python (charactere de séparation) 

    Vous allez ensuite pouvoir sélectionner les cases qui correspondent aux scripts que vous souhaitez lancer sur vos PCs. Une fois votre sélection faite vous allez pouvoir cliquer sur valider afin de valider votre sélection. A ce moment la, les lignes correspondantes aux différents scripts cochés vont être copiées du fichier "playbook.txt" vers le fichier "commande.txt" (on va simplement retirer "* nomDuProgramme"). Le fichier "commande.txt" contient maintenant les commandes "bash" permettant l'execution des différents scripts ansible sélectionnés.

    Vous pouvez donc si vous le souhaitez ajouter des playBooks dans le fichier "playbook.txt" en respectant le format utilisé(ils apparaitrons alors parmis la liste de checkboxs). Il faudra aussi créer leurs playbooks associés (.yml dans le même dossier que les autres playbooks).

    Le fichier "commande.txt" est lui réécrit à chaque validation, vous n'avez rien à écrire dedans.




    4. Explication détaillée bouton "Valider":


    Ce bouton permet de valider nos sélections et de lancer les programmes sélectionnés sur les PCs sélectionnés.

    Le script va executer les différentes commandes(scripts ansible) présentes dans le fichier "commande.txt". Ces différentes commandes vont aller chercher dans le fichier "hosts.ini" les différentes informations nécéssaires à la connection SSH sur les PC du fichier. (IP du PC, Username, Password, PasswordRoot). Le script va donc executer les différents scripts ansible sélectionnés sur tous les PC sélectionnés 1 à 1 en se connectant en SSH.

    Une fois les scripts ansible executés, les output et résultats obtenus (echec/succès) vont apparaitre dans notre champs de texte.




*************************************************    Liens utiles:    ****************************************************

Site web Ansible : https://www.ansible.com/

Site web Documentation python : https://www.python.org/doc/







------------------

Command Ansible playbook launch : /home/cyberlab/.local/bin/ansible-playbook -i /home/cyberlab/Desktop/Ansibleproject/inventory/hosts.ini  /home/cyberlab/Desktop/Ansibleproject/playbooks/playbook_programme.yml

------------


