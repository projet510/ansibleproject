#importation de tous les modules nécessaires au fonctionnements du script
import tkinter as tk
import os
from tkinter import ttk
import subprocess as sb
from tkinter import scrolledtext
from tkinter import Image
from PIL import Image,ImageTk


#Création des variables globales qui permmettent de stocker 
# la liste des pc, des playbooks et les commandes associées
playBookSelect = []
pcSelect = []
configPc = []
ip = []
playbook = []
listCommande = []

# création du bouton valider pour la liste pc qui permet d'enregistrer les pc sélectionner
def boutonValiderPC(list,parent):
    # on vide la liste des pc puis on récupère les pc sélectionnés et on les inscrit dans
    # le fichier hosts.ini qui est utilisé par Ansible
    pcSelect = []
    for i in range(len(list)):
         pcSelect.append(list[i].get())
  
    fichier = open("/home/cyberlab/ansibleproject/inventory/hosts.ini", "w")
    fichier.write("\n")
    fichier.write(line[1] + "\n")
    fichier.write("\n")

    for k in range(0, len(pcSelect)):
        if(pcSelect[k] == 1):
                fichier.write(line[k + 3] + "\n")

    fichier.close()
    parent.destroy()

# Fonction permmettant de sélectionner tous les pc d'un seul coup 
def ttSelec(listButton,listPC):
	nombreDeCoche=0
	for i in range (len(listButton)):
		nombreDeCoche+=listPC[i].get()
	if(nombreDeCoche<len(listButton)):
		for i in range (len(listButton)):
			listButton[i].select()
	else:
		for i in range (len(listButton)):
			listButton[i].deselect()
		

# Fonction pour le bouton permettant de valider les playbooks à executer
def boutonValiderPlayBook(list, parent):
    # on vide la liste de playbook et on récupère ceux sélectionnés
    playBookSelect = []
    fichier = open("commande.txt", "w")
    for i in range(len(list)):
        playBookSelect.append(list[i].get())
    # on écrit dans le fichier commande.tkt les commande permmettant de lancer chaque playbook sélectionnés
    for k in range(0, len(playBookSelect)):
        if (playBookSelect[k] == 1):
            fichier.write(listCommande[k] +"\n")
    fichier.close()
    parent.destroy()
   
#   Fonction pour afficher la liste des pc disponibles 
def menuDeroulantPC():
    # création de la fenêtre
    window2 = tk.Toplevel()
    scrollbar = tk.Scrollbar(window2)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    checklist = tk.Text(window2, width=20)
    checklist.pack()
	
    listCheckButton=[]
    listPC = []
    # on récupère chaque pc et on affiche l'ip avec un bouton checkbox associé
    for i in range(len(ip)):
        var = tk.IntVar()
        listPC.append(var)
        txt = "host" + str(i + 1) + ": " + ip[i]
        checkbutton = tk.Checkbutton(checklist, text=txt, variable=var)
        listCheckButton.append(checkbutton)
        checklist.window_create("end", window=checkbutton)
        checklist.insert("end", "\n")
        
    # mise en place de la scrollbar
    checklist.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=checklist.yview)
    
    # création du bouton tout sélectionner et du bouton valider
    checklist.configure(state="disabled")
    ttSelecButton=tk.Button(window2,text= "Tout sélectionner",command=lambda:ttSelec(listCheckButton,listPC))
    ttSelecButton.pack()
    valider = tk.Button(window2, text="Valider", command=lambda: boutonValiderPC(listPC, window2))
    valider.pack()

# Fonction pour créer la fenêtre de sélection des playbooks
def menuDeroulantPlayBook():
    # création de la fenêtre
    window2 = tk.Toplevel()
    scrollbar = tk.Scrollbar(window2)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    checklist = tk.Text(window2, width=20)
    checklist.pack()

    listPlayBook = []
    # récupération des différents playbooks et création d'un bouton checkbox associé à chacun des playbooks
    for i in range(len(playbook)):
        var = tk.IntVar()
        listPlayBook.append(var)
        txt = "host" + str(i + 1) + ": " + playbook[i]
        checkbutton = tk.Checkbutton(checklist, text=txt, variable=var)
        checklist.window_create("end", window=checkbutton)
        checklist.insert("end", "\n")
    # mise en place de la scrollbar
    checklist.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=checklist.yview)

    # création du bouton valider
    checklist.configure(state="disabled")
    valider = tk.Button(window2, text="Valider", command=lambda: boutonValiderPlayBook(listPlayBook, window2))
    valider.pack()

# fonction associée au bouton run permettant de lancer les playbooks
def bash():
    #récupération des commandes à lancer
    fichier = open("commande.txt", "r")
    lines = fichier.readlines()

    for i in range (len(lines)):
        # exécution des commandes
        listeCommande = lines[i].strip().split(" ")
        output = sb.run(listeCommande, capture_output=True ,env=my_env);
        # récupération et affichage des output des commandes dans l'encart prévu 
        scrolled_text.insert("end", output.stdout)


# récupération des adresses ip dans le fichier list_hosts.ini 
with open("/home/cyberlab/ansibleproject/inventory/list_hosts.ini", "r") as file:
    line = file.read().splitlines()

# mise en forme des lignes afin de n'avoir que les adresses ip 
for i in range(3, len(line)):
    configPc.append(line[i].split(" "))

for j in range(0, len(configPc)):
    ipAd = configPc[j][1].split('=')
    ip.append(ipAd[1])

# récupération des playbooks disponibles dans playbook.tkt
with open("playbook.txt", "r") as file2:
    lines = file2.read().splitlines()

for i in range(0, len(lines)):
    x = lines[i].split("*")
    listCommande.append(x[0])
    playbook.append(x[1])

# création de la fenêtre principale
window = tk.Tk()
window.geometry("600x600")

# initialisation des icones pour les boutons
iconAnsible=Image.open("/home/cyberlab/ansibleproject/scripts/ansible.png")
iconAnsible=iconAnsible.resize((20,20))
iconAnsible=ImageTk.PhotoImage(iconAnsible)

iconPC=Image.open("/home/cyberlab/ansibleproject/scripts/iconPC.png")
iconPC=iconPC.resize((20,20))
iconPC=ImageTk.PhotoImage(iconPC)

# création des boutons 
buttonMenuDeroulantPC = tk.Button(window, image = iconPC ,compound="left",text="liste PC", command=menuDeroulantPC)
buttonMenuDeroulantPC.grid(column=1,row=0)

buttonMenuDeroulantPlayBook = tk.Button(window,image=iconAnsible,compound="left", text="liste PlayBook", command=menuDeroulantPlayBook)
buttonMenuDeroulantPlayBook.grid(column=3,row=0)

buttonBashRun = tk.Button(window, text="Run", command=bash)
buttonBashRun.grid(column=2,row=3)

#création de la scrollbar
scrolled_text = tk.scrolledtext.ScrolledText(window)
scrolled_text.grid(columnspan=4,rowspan=6)

#démarrage de la boucle principale de la fenêtre
window.mainloop()

